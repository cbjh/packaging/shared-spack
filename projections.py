import argparse

parser = argparse.ArgumentParser()
parser.add_argument("default_arch", choices=["broadwell", "zen2"])
args = parser.parse_args()

class Config:
    default_arch: str = args.default_arch

for package_name in ["gromacs"]:
    for default_arch in [True, False]:
        for cp2k_variant in [True, False]:
            for double_variant in [True, False]:
                for mpi_variant in [True, False]:
                    for plumed_variant in [True, False]:
                        for cpu_only_variant in [True, False]:
                            if not default_arch and not cp2k_variant and not double_variant and not mpi_variant and not plumed_variant and not cpu_only_variant:
                                continue

                            key = package_name

                            if cp2k_variant:
                                key += "+cp2k"

                            if plumed_variant:
                                key += "+plumed"

                            if mpi_variant:
                                key += "+mpi"

                            if double_variant:
                                key += "+double"

                            if cpu_only_variant:
                                key += "~cuda"

                            if default_arch:
                                key += f" target={Config.default_arch}"

                            if default_arch:
                                value = "{name}"
                            else:
                                value = "{architecture.target}/{name}"

                            if mpi_variant:
                                value += "-mpi"

                            if cpu_only_variant:
                                value += "-cpu"

                            if double_variant:
                                value += "-double"

                            if cp2k_variant:
                                value += "-cp2k"

                            if plumed_variant:
                                value += "-plumed"

                            value += "/{version}"

                            if cp2k_variant:
                                value += "/{^cp2k.version}"

                            if plumed_variant:
                                value += "/{^plumed.version}"

                            value += "/{/hash:8}"

                            print(f'        "{key}": "{value}"')

for package_name in ["gromacs-swaxs", "gromacs-chain-coordinate"]:
    for default_arch in [True, False]:
        for double_variant in [True, False]:
            for mpi_variant in [True, False]:
                for cpu_only_variant in [True, False]:
                    if not default_arch and not double_variant and not mpi_variant and not cpu_only_variant:
                        continue

                    key = package_name

                    if mpi_variant:
                        key += "+mpi"

                    if double_variant:
                        key += "+double"

                    if cpu_only_variant:
                        key += "~cuda"

                    if default_arch:
                        key += f" target={Config.default_arch}"

                    if default_arch:
                        value = "{name}"
                    else:
                        value = "{architecture.target}/{name}"

                    if mpi_variant:
                        value += "-mpi"

                    if cpu_only_variant:
                        value += "-cpu"

                    if double_variant:
                        value += "-double"

                    value += "/{version}"

                    value += "/{/hash:8}"

                    print(f'        "{key}": "{value}"')

print('        "target=' + Config.default_arch + '": "{name}/{version}/{/hash:8}"')
print('        "all": "{architecture.target}/{name}/{version}/{/hash:8}"')
