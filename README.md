# SSH

Follow https://cbjh.gitlab.io/hints/technical/clusters/accessing-clusters

# Clone

Location depends on the cluster
- `/data/shared/spack/0.22.1+240909` on Smaug
- `/groups/CBG/opt/spack-0.21.1` on Elwe
- `/home/global/group-jh/shared-spack/0.19.0` on Snowden

```shell
$ ssh {hostname}
$ git clone --recurse-submodules git@gitlab.com:cbjh/packaging/shared-spack.git {location}
```

`{hostname}` is one of `smaug`, `elwe1`, `snowden2`.

# VSCode

The most comfortable way to edit configuration is to use VSCode.

1. Install VSCode.
2. Install _Remote - SSH_ extension from VSCode.
3. Click in the bottom left corner to connect to `snowden`, `elwe1`, `smaug`.
4. Open remote `{location}` directory.

# Bash

## Smaug

Add following to your `.bashrc` and reload terminal

```bash
source /data/shared/spack/0.22.1+240909/shared.bash
```

## Elwe

Add following to your `.bashrc` and reload terminal

```bash
source /groups/CBG/opt/spack-0.21.0/shared.bash
```

## Snowden

```bash
source /home/global/group-jh/shared-spack/0.19.0/shared.bash
```

# Configuration

Create following symlinks

```shell
$ cd config
$ ln -s config-{name}.yaml config.yaml
$ ln -s packages-{name}.yaml packages.yaml
$ ln -s modules-{name}.yaml modules.yaml
```

`{name}` is one of `smaug`, `elwe`, `snowden`.

# Compiler

## Smaug

```shell
$ shared-spack-fix-permissions
$ shared-spack install gcc@11.4.0+binutils gcc@9.5.0+binutils
$ shared-spack load gcc@11.4.0 gcc@9.5.0
$ shared-spack compiler find
```

## Elwe

```bash
shared-spack-fix-permissions
shared-spack bootstrap disable github-actions-v0.5
```

```bash
sbatch --partition=uds-hub --constraint=XEON_E5_2630v4 \
    --mem-per-cpu=900MB --nodes 1 --ntasks-per-node=10 --time=1:30:00 \
    --wrap "bash -c 'source /groups/CBG/opt/spack-0.21.0/shared.bash && source install-broadwell-gcc.bash'"
```

```bash
tail -f slurm-*
```

## Snowden

```shell
$ shared-spack-fix-permissions
$ shared-spack install gcc@12.2.0+binutils
$ shared-spack load gcc@12.2.0
$ shared-spack compiler find
```

## Afterwards

Make sure that the system compiler is not used in `{location}/root/etc/spack/linux/compilers.yaml`.

# Packages

## Smaug and Snowden

Install packages, the commands are [on the wiki](https://gitlab.com/cbjh/wiki/-/wikis/Spack#packages-installed-on-cluster).

## Kaiserslautern

Install packages from [the wiki](https://gitlab.com/cbjh/wiki/-/wikis/Spack#packages-installed-on-cluster).

Example installation of a package

```bash
$ sbatch --partition=uds-hub --constraint=XEON_E5_2630v4 \
    --mem-per-cpu=1GB --nodes 1 --ntasks-per-node=10 --time=4:00:00 \
    --wrap "bash -c 'source /groups/CBG/opt/spack-0.19.1/shared.bash && shared-spack install --no-checksum gromacs@2022.5'"
$ tail -f slurm-*
```

To install all packages from the list put commands into a `packages.bash` file

```bash
$ sbatch --partition=uds-hub --constraint=XEON_E5_2630v4 \
    --mem-per-cpu=1GB --nodes 1 --ntasks-per-node=10 --time=6:00:00 \
    --wrap "bash -c 'source /groups/CBG/opt/spack-0.19.1/shared.bash && source packages.bash'"
$ tail -f slurm-*
```

# Correct permissions

Needed when using Git or VSCode remote connection

```bash
$ shared-spack-fix-permissions
```

# Notify

Send a message on Slack to notify the group.

# Known issues

## Smaug

As of 12.09.2024

- glibc 2.39 present in current Arch Linux is not compatible with older software and cannot be replaced

  ```
     1185    In file included from /usr/include/linux/fs.h:19,
     1186                     from /tmp/w8jcik/spack-stage/spack-stage-cmake-3.26.6-i42etjuwfpvkmbtsusj6k2kpm5frrz4l/spack-src/Utilities/cmlibarchive/libarchive/archive_read_disk_posix.c:56:
  >> 1187    /usr/include/linux/mount.h:96:6: error: redeclaration of 'enum fsconfig_command'
     1188       96 | enum fsconfig_command {
     1189          |      ^~~~~~~~~~~~~~~~
     1190    In file included from /tmp/w8jcik/spack-stage/spack-stage-cmake-3.26.6-i42etjuwfpvkmbtsusj6k2kpm5frrz4l/spack-src/Utilities/cmlibarchive/libarchive/archive_read_disk_posix.c:38:
     1191    /data/shared/spack/0.22.1+240909/root/opt/spack/linux-archrolling-broadwell/gcc-9.5.0/gcc-11.4.0-mjmmnhaumnwgkrxopuvmdqyrfsvqezmc/lib/gcc/x86_64-pc-linux-gnu/11.4.0/include-fixed/sys/mount.h:250:6: note: originally defined here
     1192      250 | enum fsconfig_command
     1193          |      ^~~~~~~~~~~~~~~~
  ```

  Workaround from https://github.com/spack/spack/issues/32675#issuecomment-1488495042

  ```
  cd /data/shared/spack/0.22.1+240909/root/opt/spack/linux-archrolling-broadwell/gcc-9.5.0/gcc-11.4.0-mjmmnhaumnwgkrxopuvmdqyrfsvqezmc/lib/gcc/x86_64-pc-linux-gnu/11.4.0/include-fixed/sys
  mv mount.h mount.h~
  ```
- Use GCC 9.5.0 to build GCC 11.4.0. System GCC is able to build GCC 9, but not 11.

## Snowden

As of 11.11.2022

- System GCC 12 is missing Fortran compiler, which is needed by some dependencies of GROMACS.
- glibc 2.36 present in current Arch Linux is not compatible with older software and cannot be replaced
  ```
  $ pacman -Q glibc
  glibc 2.36-6
  ```
  Example error during build of GCC, most likely other software will have similar issues
  ```
  In file included from /usr/include/linux/fs.h:19,
                  from /tmp/w8jcik/spack-stage/spack-stage-gcc-8.5.0-hhetojkwouilahrgl4b4wyuffbhm4afu/spack-src/libsanitizer/sanitizer_common/sanitizer_platform_limits_posix.cc:68:
  /usr/include/linux/mount.h:95:6: error: multiple definition of 'enum fsconfig_command'
  enum fsconfig_command {
        ^~~~~~~~~~~~~~~~
  In file included from /tmp/w8jcik/spack-stage/spack-stage-gcc-8.5.0-hhetojkwouilahrgl4b4wyuffbhm4afu/spack-src/libsanitizer/sanitizer_common/sanitizer_platform_limits_posix.cc:55:
  /tmp/w8jcik/spack-stage/spack-stage-gcc-8.5.0-hhetojkwouilahrgl4b4wyuffbhm4afu/spack-src/spack-build/./gcc/include-fixed/sys/mount.h:249:6: note: previous definition here
  enum fsconfig_command
        ^~~~~~~~~~~~~~~~
  In file included from /usr/include/linux/fs.h:19,
                  from /tmp/w8jcik/spack-stage/spack-stage-gcc-8.5.0-hhetojkwouilahrgl4b4wyuffbhm4afu/spack-src/libsanitizer/sanitizer_common/sanitizer_platform_limits_posix.cc:68:
  /usr/include/linux/mount.h:129:8: error: redefinition of 'struct mount_attr'
  struct mount_attr {
          ^~~~~~~~~~
  In file included from /tmp/w8jcik/spack-stage/spack-stage-gcc-8.5.0-hhetojkwouilahrgl4b4wyuffbhm4afu/spack-src/libsanitizer/sanitizer_common/sanitizer_platform_limits_posix.cc:55:
  /tmp/w8jcik/spack-stage/spack-stage-gcc-8.5.0-hhetojkwouilahrgl4b4wyuffbhm4afu/spack-src/spack-build/./gcc/include-fixed/sys/mount.h:219:8: note: previous definition of 'struct mount_attr'
  struct mount_attr
          ^~~~~~~~~~
  ```
- GCC 11, 10, 9 doesn't build through Spack without [this workaround](https://github.com/spack/spack/issues/32675#issuecomment-1249351437).  
  With the workaround, older GCC builds, but later other packages like `cmake` still fail. It means that the workaround is not really useful and only GCC 12 is usable so far.
- `PIP_REQUIRE_VIRTUALENV` is set on the cluster, which breaks installation `py-` packages. Variable needs to be unset.
- Module system is missing.
