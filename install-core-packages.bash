shared-spack install byobu                                    # byobu
shared-spack install gromacs-chain-coordinate@2021.5-0.3 ^cmake@:3.26  # gromacs-chain-coordinate/2021.5-0.3
shared-spack install gromacs-chain-coordinate@2021.5-0.3+mpi ^cmake@:3.26  # gromacs-chain-coordinate-mpi/2021.5-0.3
shared-spack install gromacs-swaxs@2021.7-0.5                 # gromacs-swaxs/2021.7-0.5
shared-spack install gromacs-swaxs@2021.7-0.5+mpi             # gromacs-swaxs-mpi/2021.7-0.5
shared-spack install gromacs@2021                             # gromacs/2021.7
shared-spack install gromacs@2021+mpi                         # gromacs-mpi/2021.7
shared-spack install gromacs@2022                             # gromacs/2022.6
shared-spack install gromacs@2022+mpi                         # gromacs-mpi/2022.6
shared-spack install gromacs@2023                             # gromacs/2023.4
shared-spack install gromacs@2023+mpi                         # gromacs-mpi/2023.4
shared-spack install --no-checksum gromacs@=2024.1            # gromacs/2024.1
shared-spack install --no-checksum gromacs@=2024.1+mpi        # gromacs-mpi/2024.1
shared-spack install htop                                     # htop
shared-spack install py-mdanalysis                            # py-mdanalysis py-scipy py-numpy py-matplotlib
shared-spack install snakemake@7                              # snakemake
